# Kotlin Spring Caching

### Run locally
```
git clone https://gitlab.com/hendisantika/kotlin-spring-caching.git 
```

```
gradle clean bootRun --info
```

### Screen shot

#### Home Page

![Home Page](img/home.png "Home Page")

#### List Page

![List Page](img/list.png "List Image Page")